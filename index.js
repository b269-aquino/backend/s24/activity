const num = 2;
const getCube = num ** 3;
console.log(`The cube of ${num} is ${getCube}`);

const address = ["258", "Washingtong Ave. NW,", "California", "90011"];

const [addressNumber, addressStreet, addressCity, addressCode] = address;

console.log(`I live at ${addressNumber} ${addressStreet} ${addressCity} ${addressCode} `);


const animal = {
	animalName: "Lolong",
	animalType: "saltwater crocodile",
	animalWeight: "1075 kgs",
	animalMeasurement: "20ft 3in"
};

const {animalName, animalType, animalWeight, animalMeasurement} = animal;

console.log(`${animalName} was a ${animalType}. He weighed at ${animalWeight} with a measurement of ${animalMeasurement}.`);


const arrayNumbers = [2, 4, 6, 8, 10];

arrayNumbers.forEach((number) => {
	console.log(number)
});

const reduceNumber = arrayNumbers;

console.log(reduceNumber.reduce((sum, num) => sum + num))


class Dog {
	constructor (name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
};

const myDog = new Dog();
myDog.name = "Jarvis",
myDog.age = 4,
myDog.breed = "Pomeranian-Husky",
console.log(myDog);